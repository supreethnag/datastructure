#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
using namespace std;
int main() {
   
    string s;
    char ref;
    int odd=0,count;
    cin>>s;
     
    int flag = 1;
    // Assign Flag a value of 0 or 1 depending on whether or not you find what you are looking for, in the given string 
    std::sort(s.begin(),s.end());
    ref=s[0];
    count=1;
    for(int i=1;i<s.length();i++)
    {
    	if(s[i]==ref)
	{
		count++;
	}
	else
	{
		ref=s[i];
		if(count%2!=0)
			odd++;
		count=1;
	}
	if(odd>1)
	{
		flag=0;
		break;
	}
    }	
    if(flag==0)
        cout<<"NO";
    else
        cout<<"YES";
    return 0;
}

