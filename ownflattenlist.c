#include<stdio.h>
#include<stdlib.h>
struct node
{
	int data;
	struct node *right;
	struct node *down;
};

void printlist(struct node *head)
{
	while(head!=NULL)	
	{
		printf("%d ",head->data);
		head=head->down;
	}
	printf("\n");
}

void push(struct node **headref,int new_data)
{
	struct node *newnode=(struct node *)malloc(sizeof(struct node));
	newnode->data=new_data;
	newnode->right=NULL;
	newnode->down=(*headref);
	(*headref)=newnode;
}

struct node *merge(struct node *a,struct node *b)
{
	if(a==NULL)
		return b;
	if(b==NULL)
		return a;
	struct node *result;
	if(a->data<b->data)
	{
		result=a;
		result->down=merge(a->down,b);
	}
	else
	{
		result=b;
		result->down=merge(a,b->down);
	}
	return result;
}
struct node *flatten(struct node *root)
{
	if(root==NULL || root->right==NULL)
		return root;
	return merge(root,flatten(root->right));
}
void main()
{
	struct node *root=NULL;
	push( &root, 30 );
	push( &root, 8 );
   	push( &root, 7 );
    	push( &root, 5 );
 
    	push( &( root->right ), 20 );
    	push( &( root->right ), 10 );
 
    	push( &( root->right->right ), 50 );
    	push( &( root->right->right ), 22 );
    	push( &( root->right->right ), 19 );
 
   	push( &( root->right->right->right ), 45 );
    	push( &( root->right->right->right ), 40 );
    	push( &( root->right->right->right ), 35 );
    	push( &( root->right->right->right ), 28 );
	root=flatten(root);
	printlist(root);
}

