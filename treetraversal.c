#include<stdio.h>
#include<stdlib.h>
struct node 
{
	int data;
	struct node *left;
	struct node *right;
};

void printarray(int ints[], int len)
{
  int i;
  for (i=0; i<len; i++) {
    printf("%d ", ints[i]);
  }
  printf("\n");
} 

void printpathrecur(struct node *node,int path[],int pathlen)
{
	if(node==NULL)
		return;
	 path[pathlen] = node->data;
 	 pathlen++;
	if(node->left==NULL && node->right==NULL)
		printarray(path,pathlen);
	else
	{
		printpathrecur(node->left,path,pathlen);
		printpathrecur(node->right,path,pathlen);
	}
}
struct node* lca(struct node *root,int n1,int n2)
{
	if(root->data < n1 && root->data < n2)
		lca(root->right,n1,n2)
	if(root->data > n1 && root->data > n2)
		lca(root->left,n1,n2);
	return root;		
}
int size(struct node *root,int count)
{
	if(root==NULL)
		return count;
	count++;
	count=size(root->left,count);
	count=size(root->right,count);
	return count;
}


struct node* newNode(int data)
{
     struct node* node = (struct node*)
                                  malloc(sizeof(struct node));
     node->data = data;
     node->left = NULL;
     node->right = NULL;
 
     return(node);
}
void inorder(struct node *root)
{
	if(root==NULL)
		return;
	inorder(root->left);
	printf("%d ",root->data);
	inorder(root->right);
}

void preorder(struct node *root)
{
	if(root==NULL)
		return;
	
	printf("%d ",root->data);
	preorder(root->left);
	preorder(root->right);
}

void postorder(struct node *root)
{
	if(root==NULL)
		return;
	postorder(root->left);
	postorder(root->right);
	printf("%d ",root->data);
}

int identicaltrees(struct node *a,struct node *b)
{
	if(a==NULL && b==NULL)
		return 1;
	if(a->data==b->data)
	{
		identicaltrees(a->left,b->left);
		identicaltrees(a->right,b->right);
	}
	else 
	{
		return 0;
	}
}
int maxdepth(struct node *root)
{
	if(root==NULL)
		return 0;
	else
	{
		int ldepth=maxdepth(root->left);
		int rdepth=maxdepth(root->right);
		
		if(ldepth>rdepth)
			return(ldepth+1);
		else
			return(rdepth+1);
	}
}
void mirror(struct node *root)
{
	if(root==NULL)
		return;
	mirror(root->left);
	mirror(root->right);
	struct node *temp=root->left;
        root->left=root->right;
        root->right=temp;
}
void main()
{
     struct node *root  = newNode(1);
     struct node *root2 = newNode(1);
     int pathlen=0;
     int path[1000];
     root->left             = newNode(2);
     root->right           = newNode(3);
     root->left->left     = newNode(4);
     root->left->right   = newNode(5); 
	
	printpathrecur(root,path,pathlen);
	
	       
     

	printf("\n Preorder traversal of binary tree is \n");
     preorder(root);
 
     printf("\n Inorder traversal of binary tree is \n");
     inorder(root);  
 
     printf("\n Postorder traversal of binary tree is \n");
     postorder(root);
	
	printf("\n size of the tree is %d \n",size(root,0));
	 root2->left = newNode(2);
    root2->right = newNode(3);
    root2->left->left = newNode(4);
    root2->left->right = newNode(5); 
 
    if(identicaltrees(root, root2))
        printf("\n Both tree are identical. \n");
    else
        printf("\n Trees are not identical.\n");
	
	printf("\n depth of the tree is %d \n",maxdepth(root));
	mirror(root);
	
	preorder(root);

     printf("\n Inorder traversal of binary tree is \n");
     inorder(root);

     printf("\n Postorder traversal of binary tree is \n");
     postorder(root);
	
}						
