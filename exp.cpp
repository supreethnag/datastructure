#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


void swap(long* a, long* b)
{
    long t = *a;
    *a = *b;
    *b = t;
}
void quicksort(vector <long>  &a, int l,int r) {
    if(l>=r)
        return ;

    int p = a[r];
    int i=l-1;
    for(int j=l;j<r;j++)
    {
        if(a[j]<=p)    
        {
            i++;
            swap(&a[i], &a[j]);
        }
            
    }
     swap(&a[i+1], &a[r]);
    quicksort(a,l,i);
    quicksort(a,i+2,r);
}





int main() {
    int n;
    cin>>n;
    int i,j,k;
     vector<long> a;
   
    for(i=0;i<n;i++)
	{
		long tmp;
        cin>>tmp;
	a.push_back(tmp);
    }
    quicksort(a,0,a.size()-1);
    
	
	long least=a[n-1],diff;
    	for(i=0;i<n-1;i++)
 	{
		diff=a[i+1]-a[i];
		if(diff<least)
			least=diff;
	}
	
	
    for(i=0;i<n-1;i++)
    {
		if(a[i+1]-a[i]==least)
			cout<<a[i]<<" "<<a[i+1]<<" ";
    }		
    return 0;
}

