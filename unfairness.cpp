#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


// It is NOT mandatory to use the provided template. You can handle the IO section differently.

int main() {
    /* The code required to enter n,k, candies is provided*/

    int i,j,N, K, unfairness;
    cin >> N >> K;
    int *candies;
    candies=new int[N];
    for (i=0; i<N; i++)
        cin>>candies[i];
	
    /** Write the solution code here. Compute the result, store in  the variable unfairness --
    and output it**/
    int tmp;
    for(i=0;i<N;i++)
    {
    	for(j=i+1;j<N;j++){
        	if( candies[i] > candies[j])
        	{
                	tmp =candies[i];
                	candies[i] = candies[j];
                	candies[j] = tmp;
        	}
    	}
    }
   
    unfairness=candies[K-1]-candies[0];
    for(i=1;i<N-K-1;i++)
    {
	if(unfairness>candies[i+K-1]-candies[i])
		unfairness=candies[i+K-1]-candies[i];
    }
    cout<<unfairness;
    return 0;
}

