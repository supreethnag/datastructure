package utils;

import java.util.Arrays;

public class Util {

    public static int getMax(int[] arr) {
        int max = arr[0];
        for (int i = 0; i < arr.length; ++i) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }

        return max;
    }

    public static int getMin(int[] arr) {
        int min = arr[0];
        for (int i = 0; i < arr.length; ++i) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }

        return min;
    }
    
    public static String print(int[] arr) {
        return  Arrays.toString(arr);
    }
    
    public static void swap(int arr[], int i , int j) {
        int temp = arr[i];
        arr[i]= arr[j];
        arr[j] = temp;
    }

    public static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a%b);
    }
}
