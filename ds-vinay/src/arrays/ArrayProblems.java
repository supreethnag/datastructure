/**
 * 
 */
package arrays;

import java.util.Arrays;
import java.lang.Math;

import utils.Util;

public class ArrayProblems {

    public void findPairHash(int[] arr, int x) {
        int max = Util.getMax(arr);
        int[] hash = new int[max + 1];
        for (int i = 0; i < arr.length; ++i) {
            hash[arr[i]] = 10;
        }
        for (int i = 0; i < arr.length - 1; ++i) {
            int search = x - arr[i];
            if (search < 0) {
                continue;
            }
            if (hash[search] == 10) {
                System.out.println("Found the pair " + search + "---" + arr[i]
                        + " for sum " + x);
                return;
            }
        }
        System.out.println(" Could not find the pair for the sum " + x);
    }

    public void findPairSort(int[] array, int x) {
        Arrays.sort(array);
        int l = 0;
        int h = array.length - 1;
        while (l < h) {
            int curr = array[l] + array[h];
            if (curr == x) {
                System.out.println("Found pair : " + array[l] + " - "
                        + array[h] + " for sum " + x);
                return;
            }
            if (curr < x) {
                l++;
            } else {
                h--;
            }
        }
        System.out.println(" Could not find the pair for the sum " + x);
    }

    /*
     * 1. Write a C program that, given an array A[] of n numbers and another
     * number x, determines whether or not there exist two elements in S whose
     * sum is exactly x.
     */
    public void test1() {
        int[] array = { 30, 50, 30, 20, 10 };
        int x = 30;

        this.findPairHash(array, x);

        int[] array1 = { 10, 50, 30, 20, 10, 90 };
        x = 31;
        this.findPairHash(array1, x);

    }

    public void findMajorityEle(int arr[]) {
        int maxIndex = 0, count = 1;
        for (int i = 0; i < arr.length; ++i) {
            if (arr[maxIndex] == arr[i]) {
                count++;
            } else {
                count--;
            }
            if (count == 0) {
                maxIndex = i;
                count = 1;
            }
        }

        count = 0;
        for (int i = 0; i < arr.length; ++i) {
            if (arr[i] == arr[maxIndex]) {
                count++;
            }
        }
        if (count > arr.length / 2) {
            System.out.println("Majority element is " + arr[maxIndex]);
            return;
        }
        System.out.println("None");
    }

    /*
     * Majority Element: A majority element in an array A[] of size n is an
     * element that appears more than n/2 times (and hence there is at most one
     * such element).
     * 
     * Write a function which takes an array and emits the majority element (if
     * it exists), otherwise prints NONE
     */
    public void test2() {
        int a[] = { 1, 3, 3, 1, 2 };
        this.findMajorityEle(a);

        int a1[] = { 3, 3, 3, 1, 2 };
        this.findMajorityEle(a1);

        int a2[] = { 3, 3, 3, 2, 2, 2 };
        this.findMajorityEle(a2);

        int a3[] = { 3, 3, 3, 2, 2, 2, 3 };
        this.findMajorityEle(a3);
    }

    /*
     * Given an array of positive integers. All numbers occur even number of
     * times except one number which occurs odd number of times. Find the number
     * in O(n) time & constant space.
     */

    public void findNumberWithOddCount(int[] arr) {

        int num = arr[0];
        for (int i = 1; i < arr.length; ++i) {
            num ^= arr[i];
        }
        System.out.println("Number is " + num);
    }

    public void test3() {
        int a[] = { 1, 3, 3, 1, 2 };
        this.findNumberWithOddCount(a);

        int a1[] = { 3, 3, 3, 2, 2 };
        this.findNumberWithOddCount(a1);

        int a2[] = { 3, 3, 3, 2, 2 };
        this.findNumberWithOddCount(a2);

        int a3[] = { 3, 3, 3, 2, 2, 2, 3 };
        this.findNumberWithOddCount(a3);

        int a4[] = { 3 };
        this.findNumberWithOddCount(a4);

    }

    public void findMaxSubArraySum(int[] arr) {
        int currMax = arr[0], max = 0;
        for (int i = 1; i < arr.length; ++i) {
            currMax = Math.max(arr[i], currMax + arr[i]);
            max = Math.max(currMax, max);
        }
        System.out.println("Max sum = " + max);
    }

    /*
     * Write an efficient C program to find the sum of contiguous subarray
     * within a one-dimensional array of numbers which has the largest sum.
     */

    public void test4() {
        this.findMaxSubArraySum(new int[] { 3, 4, 5, 6, -7, 8, -2, 10 });
        this.findMaxSubArraySum(new int[] { 1, -1, 1, -1 });
        this.findMaxSubArraySum(new int[] { 1, 1, 1, 1 });
        this.findMaxSubArraySum(new int[] { 10, -1, 1, -1 });
        this.findMaxSubArraySum(new int[] { 10, -1, -1, 1 });
    }

    public void findMissingNum(int[] arr) {
        int sum = 0;
        int n = arr.length + 1;
        int actualSum = (n * (n + 1)) / 2;
        for (int i = 0; i < n - 1; ++i) {
            sum += arr[i];
        }
        System.out.println("Missing number is " + (actualSum - sum));
    }

    /*
     * You are given a list of n-1 integers and these integers are in the range
     * of 1 to n. There are no duplicates in list. One of the integers is
     * missing in the list. Write an efficient code to find the missing integer.
     */

    public void test5() {
        this.findMissingNum(new int[] { 1, 2, 3 });
        this.findMissingNum(new int[] { 1, 2, 4, 5, 6 });
    }

    public int binarySearch(int arr[], int l, int h, int num) {
        if (l == h)
            return l;
        if (h < l)
            return -1;
        int mid = (l + h) / 2;
        if (arr[mid] == num)
            return mid;
        if (arr[mid] < num)
            return binarySearch(arr, mid + 1, h, num);
        return binarySearch(arr, l, mid - 1, num);
    }

    public void findElementInPivotedArray(int[] arr, int num) {
        int pivot = this.findPivot(arr, 0, arr.length - 1);
        System.out.println("Pivot is " + pivot);
        int index = -1;
        if (arr[pivot] == num) {
            index = pivot;
        } else if (arr[0] <= num) {
            index = binarySearch(arr, 0, pivot - 1, num);

        } else {
            index = binarySearch(arr, pivot + 1, arr.length - 1, num);
        }
        if (index == -1) {
            System.out.println("Could not find " + num + " in array");
            return;
        }
        System.out.println("Found " + num + " in array at pos " + index);
    }

    public int findPivot(int[] arr, int l, int h) {

        if (h < l)
            return -1;
        if (h == l)
            return l;

        int mid = (l + h) / 2;
        if (mid < h && arr[mid] > arr[mid + 1]) {
            return mid;
        }
        if (mid > l && arr[mid] < arr[mid - 1]) {
            return mid - 1;
        }
        if (arr[l] >= arr[mid]) {
            return findPivot(arr, l, mid - 1);
        }
        return findPivot(arr, mid + 1, h);
    }

    /*
     * An element in a sorted array can be found in O(log n) time via binary
     * search. But suppose I rotate the sorted array at some pivot unknown to
     * you beforehand. So for instance, 1 2 3 4 5 might become 3 4 5 1 2. Devise
     * a way to find an element in the rotated array in O(log n) time.
     */
    public void test6() {
        this.findElementInPivotedArray(new int[] { 3, 4, 5, 1, 2 }, 1);
        this.findElementInPivotedArray(new int[] { 6, 1, 2, 3, 4, 5 }, 6);

    }

    public void mergAndSortArrays(int arr[], int[] arr2) {
        int s = arr.length;
        int n = arr2.length;
        int curr = s - 1;
        // move the elements to the right side in the big array.
        for (int j = s - 1; j >= 0; j--) {
            if (arr[j] != 0) {
                arr[curr--] = arr[j];
            }
        }
        // curr points to the starting pos of the m array elments.
        curr++;
        // now merge them.
        int i = 0;
        int a1 = curr;
        int a2 = 0;
        while (a1 < s && a2 < n) {
            if (arr2[a2] >= arr[a1]) {
                arr[i++] = arr[a1];
                a1++;
            } else {
                arr[i++] = arr2[a2];
                a2++;
            }
        }
        while (a1 < s) {
            arr[i++] = arr[a1++];
        }
        while (a2 < n) {
            arr[i++] = arr2[a2++];
        }

        System.out.println("Final array is " + Util.print(arr));
    }

    /*
     * There are two sorted arrays. First one is of size m+n containing only m
     * elements. Another one is of size n and contains n elements. Merge these
     * two arrays into the first array of size m+n such that the output is
     * sorted.
     */

    public void test7() {
        this.mergAndSortArrays(new int[] { 1, 0, 2, 5, 0, 6 },
                new int[] { 3, 4 });
        this.mergAndSortArrays(new int[] { 1, 0, 2, 0, 0, 6, 0, 0 }, new int[] {
                3, 4, 5, 9, 10 });
        this.mergAndSortArrays(new int[] { 1, 0 }, new int[] { 3 });
        this.mergAndSortArrays(new int[] { 0, 1 }, new int[] { 3 });
        this.mergAndSortArrays(new int[] { 1, 0, 0 }, new int[] { 0, 3 });
    }

    // Returns the median.
    public float getMedian(int[] arr, int l, int h) {
        int len = h - l + 1;
        int mid = (l + h) / 2;
        if (len % 2 == 0) {
            return arr[mid];
        }
        int median = (arr[mid] + arr[mid - 1]) / 2;
        return median;

    }

    public void getMedianOfTwoArrays(int[] a1, int a2[]) {

        if (a1.length == a2.length && a1.length == 2) {
            float median = Math.max(a1[0], a1[0]) + Math.min(a1[1], a2[1]);
            median /= 2;
            System.out.println("Median of two arrays is " + median);
            return;
        }
        float m1 = this.getMedian(a1, 0, a1.length - 1);
        float m2 = this.getMedian(a2, 0, a2.length - 1);
        if (m1 == m2) {
            System.out.println("Median of two arrays is " + m1);
            return;
        }

        if (m1 > m2) {
            this.getMedianOfTwoArrays(
                    Arrays.copyOfRange(a1, 0, a1.length / 2 + 1),
                    Arrays.copyOfRange(a2, a2.length / 2, a2.length));
            return;
        }

        this.getMedianOfTwoArrays(
                Arrays.copyOfRange(a1, a1.length / 2, a1.length),
                Arrays.copyOfRange(a2, 0, a2.length / 2 + 1));
    }

    /*
     * : There are 2 sorted arrays A and B of size n each. Write an algorithm to
     * find the median of the array obtained after merging the above 2
     * arrays(i.e. array of length 2n). The complexity should be O(log(n))
     */
    public void test8() {
        this.getMedianOfTwoArrays(new int[] { 1, 12, 15, 26, 38 }, new int[] {
                2, 13, 17, 30, 45 });
        this.getMedianOfTwoArrays(new int[] { 4, 5, 6 }, new int[] { 1, 2, 3 });
        this.getMedianOfTwoArrays(new int[] { 4, 5 }, new int[] { 1, 2 });
    }

    public void reverseArray(int a[]) {
        int l = 0, h = a.length - 1;
        while (l < h) {
            Util.swap(a, l++, h--);
        }

        System.out.println(" reversed array is " + Util.print(a));
    }

    /*
     * program to reverse an array
     */
    public void test9() {
        this.reverseArray(new int[] { 1, 2 });
        this.reverseArray(new int[] { 1 });
        this.reverseArray(new int[] { 1, 2, 3 });
        this.reverseArray(new int[] {});
    }

    public void leftRotateArray(int a[], int d) {
        int gcd = Util.gcd(a.length, d);
        for (int i = 0; i < gcd; ++i) {
            int tmp = a[i];
            int j = i, k = 0;
            while (true) {
                k = j + d;
                if (k >= a.length) {
                    k = k - a.length;
                }
                if (k == i) {
                    break;
                }
                a[j] = a[k];
                j = k;
            }
            a[j] = tmp;
        }
        System.out.println("Left rotated array is " + Util.print(a));
    }

    /*
     * Write a function rotate(ar[], d, n) that rotates arr[] of size n by d
     * elements.
     */

    public void test10() {
        this.leftRotateArray(new int[] { 1, 3, 4, 5, 6 }, 2);
        this.leftRotateArray(new int[] { 1, 3, 4, 5 }, 2);
        this.leftRotateArray(new int[] { 1, 3, 4, 5, 6 }, 1);
        this.leftRotateArray(new int[] { 1, 3 }, 2);
    }

    public void findMaxNonAdjSum(int[] a) {
        int inc = a[0], exc = 0;
        for (int i = 1; i < a.length; ++i) {
            int exc_new = Math.max(inc, exc);
            inc = (exc + a[i]);
            exc = exc_new;
        }

        System.out.println("maximum sum = " + Math.max(inc, exc));
    }

    /*
     * Given an array of positive numbers, find the maximum sum of a subsequence
     * with the constraint that no 2 numbers in the sequence should be adjacent
     * in the array. So 3 2 7 10 should return 13 (sum of 3 and 10) or 3 2 5 10
     * 7 should return 15 (sum of 3, 5 and 7).Answer the question in most
     * efficient way.
     */
    public void test11() {

        int[][] array = { { 1, 2, 3, 4 }, { 5, 5, 10, 100, 10, 5 },
                { 5, 5, 10, 40, 50, 35 } };
        for (int i = 0; i < array.length; ++i) {
            this.findMaxNonAdjSum(array[i]);
        }
    }

    /*
     * Write a program to print all the LEADERS in the array. An element is
     * leader if it is greater than all the elements to its right side. And the
     * rightmost element is always a leader. For example int the array {16, 17,
     * 4, 3, 5, 2}, leaders are 17, 5 and 2.
     */

    public void findLeaders(int a[]) {
        int n = a.length - 1;
        int maxSoFar = a[n];
        System.out.print("\n Leaders of the array are: " + maxSoFar);
        for (int i = n - 1; i >= 0; i--) {
            if (a[i] > maxSoFar) {
                System.out.print(" " + a[i]);
                maxSoFar = a[i];
            }
        }
    }

    public void test12() {

        int[][] array = { { 16, 17, 4, 3, 5, 2 }, { 4, 3, 2, 1 },
                { 100, 99, 4, 3, 54, 3 }, { 1, 2, 3, 4 } };
        for (int i = 0; i < array.length; ++i) {
            this.findLeaders(array[i]);
        }
    }
    

    public static void main(String[] args) {

        try {
            ArrayProblems p1 = new ArrayProblems();
            p1.test12();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
