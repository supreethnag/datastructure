#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;



vector<int> quicksort(vector <int>  a, int a_size) {
    if(a_size<2)
        return a;
    
    int p = a[0];
    vector<int> p1;
    vector<int> p2;
    for (int i = 1; i < a_size; i++) {
        if (a[i] < p)
            p1.push_back(a[i]);
        else if (a[i] > p)
            p2.push_back(a[i]);
    }
    p1=quicksort(p1,p1.size());
    p2=quicksort(p2,p2.size());
    p1.push_back(p);
    p1.insert(p1.end(), p2.begin(), p2.end());
    for(int i=0;i<p1.size();i++)
	cout<<p1[i]<<" ";	
    cout<<endl;
    return p1;

}
int main(void) {
   vector <int>  _ar;
   int _ar_size;
cin >> _ar_size;
for(int _ar_i=0; _ar_i<_ar_size; _ar_i++) {
   int _ar_tmp;
   cin >> _ar_tmp;
   _ar.push_back(_ar_tmp); 
}

quicksort(_ar,_ar_size);
   
   return 0;
}
