#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node *right;
    struct node *down;
} ;

void push(struct node** head_ref, int new_data)
{
    /* allocate node */
    struct node* new_node = (struct node *) malloc(sizeof(struct node));
    new_node->right = NULL;
 
    /* put in the data  */
    new_node->data  = new_data;
 
    /* link the old list off the new node */
    new_node->down = (*head_ref);
 
    /* move the head to point to the new node */
    (*head_ref)    = new_node;
}

/* Function to print nodes in the flattened linked list */
void printList(struct node *node)
{
    while(node != NULL)
    {
        printf("%d ", node->data);
        node = node->down;
    }
}

struct node * merge(struct node *a,struct node *b)
{
	if(a==NULL)
		return b;
	if(b==NULL)
		return a;
	struct node *result;
	if(a->data < b->data)
	{
		result=a;
		result->down=merge(a->down,b);
	}
	else
	{
                result=b;
                result->down=merge(a,b->down);
        }
	return result;
}

struct node * flatten(struct node *root)
{
	if(root==NULL||root->right==NULL)
		return root;
	return merge( root, flatten(root->right) );	
} 
 
int main()
{
    struct node* root = NULL;
 
    push( &root, 30 );
    push( &root, 8 );
    push( &root, 7 );
    push( &root, 5 );
 
    push( &( root->right ), 20 );
    push( &( root->right ), 10 );
 
    push( &( root->right->right ), 50 );
    push( &( root->right->right ), 22 );
    push( &( root->right->right ), 19 );
 
    push( &( root->right->right->right ), 45 );
    push( &( root->right->right->right ), 40 );
    push( &( root->right->right->right ), 35 );
    push( &( root->right->right->right ), 28 );
 
    root = flatten(root);
 
    printList(root);
 
    return 0;
}
