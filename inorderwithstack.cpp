#include<iostream>
#include<stack>
#include<stdlib.h>
using namespace std;

struct node 
{
	int data;
	struct node *left,*right;
};
void inOrder(struct node *root)
{
	stack<struct node *> s1;
	struct node *cur=root;
	int done=0;
	while(!done)
	{
		if(cur)
		{	
			s1.push(cur);
			cur=cur->left;
		}
		else
		{
			if(!s1.empty())
			{
				cur=s1.top();
				s1.pop();
				cout<<cur->data<<"  ";
				cur=cur->right;
			}
			else
				done=1;
		}
	
	}
	
}


struct node* newtNode(int data)
{
  struct node* tNode = (struct node*)
                       malloc(sizeof(struct node));
  tNode->data = data;
  tNode->left = NULL;
  tNode->right = NULL;
 
  return(tNode);
}
int main(){
struct  node *root = newtNode(1);
  root->left        = newtNode(2);
  root->right       = newtNode(3);
  root->left->left  = newtNode(4);
  root->left->right = newtNode(5); 
 
  inOrder(root);
 
  getchar();
  return 0;
}
