#include<stdio.h>
#include<stdlib.h>

struct node
{
	int n;
	struct node *next;
};

void insert_front(struct node **head,int i)
{
	struct node* tmp = (struct node*) malloc(sizeof(struct node));
	tmp->next=NULL;
	tmp->next=*head;
	tmp->n=i;
	*head=tmp;
}


void insert_rear(struct node **head,int i)
{
	struct node *tmp=(struct node *) malloc(sizeof(struct node));
	tmp->n=i;
	tmp->next=NULL;
	if( *head == NULL)
	{
		*head=tmp;
		return;
	}
	struct node **x,**tmp1;
	tmp1=head;
	x=head;
	while(*x->next!=NULL){
	   *x=*x->next;
	}
	x->next=tmp;
	head=tmp1;	
}



void printList(struct node *node)
{
  while (node != NULL)
  {
     printf(" %d ", node->n);
     node = node->next;
  }
}
void print_list(struct node **node)
{
	while(*node!=NULL){
		printf(" %d ",(*node)->n);
		*node=(*node)->next;
	}	
}



int main()
{
	struct node *head=NULL;
	insert_front(&head,1);
	insert_front(&head,2);
	insert_front(&head,3);
	insert_front(&head,4);
	insert_front(&head,5);
	insert_front(&head,6);
	printList(head);
	print_list(&head);

	insert_rear(&head,7);
	insert_rear(&head,8);
	printf("rear insertion\n");
	printList(head);
  return 1;
}
