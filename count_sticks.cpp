#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int n,i,count;
    cin>>n;
    int a[n], smallest=1001;
    for(i=0;i<n;i++)
    {
        cin>>a[i];
        if(smallest>a[i])
           smallest=a[i];
    }        
    int small=1001;
    int remaining=n;
    while(remaining>0)
    {           
        count=0;
        for(i=0;i<n;i++)
        {
            if(a[i]!=0)
            {
                count++;
                a[i]-=smallest;
                if(small>a[i]&&a[i]!=0)
                    small=a[i];
                if(a[i]==0)
                    remaining--;
            }    
        }
        cout<<count<<endl;
        smallest=small;
	small=1001;
    }
    
    return 0;
}

