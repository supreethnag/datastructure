#include<stdio.h>
#include<stdlib.h>
#define MAX_Q_SIZE 500
struct node 
{
	int data;
	struct node *left,*right;
};

struct node ** createqueue(int *front,int *rear)
{
	struct node **queue=(struct node **) malloc(sizeof(struct node *)*MAX_Q_SIZE);
	*front=*rear=0;
	return queue;
}

void enqueue(struct node **queue,int *rear,struct node *new_node)
{
	queue[*rear]=new_node;
	(*rear)++;
}

struct node *dequeue(struct node **queue,int *front)
{
	(*front)++;
	return queue[*front-1];
}

int height(struct node *root)
{
	int rheight,lheight;
	if(root==NULL)
		return 0;
	else
	{
		rheight=height(root->right);
		lheight=height(root->left);

		if(lheight>rheight)
			return lheight+1;
		else
			return rheight+1;
	}
}

struct node* newnode(int data)
{
  struct node* node = (struct node*)
                       malloc(sizeof(struct node));
  node->data = data;
  node->left = NULL;
  node->right = NULL;
 
  return(node);
}


void printgivenlevel(struct node *root,int level)
{
	if(root==NULL)
		return;
	if(level==1)
		printf("%d ",root->data);
	else
	{
		printgivenlevel(root->left,level-1);
		printgivenlevel(root->right,level-1);
	}
}

void printlevelorder(struct node *root)
{
	int h=height(root);
	int i;
	for(i=1;i<=h;i++)
		printgivenlevel(root,i);
}

void printLevelOrder(struct node *root)
{
	int rear,front;
	struct node **queue=createqueue(&rear,&front);
	struct node *tempnode=root;
	while(tempnode)
	{
		printf("%d ",tempnode->data);
		if(tempnode->left)
			enqueue(queue,&rear,tempnode->left);
		if(tempnode->right)
                        enqueue(queue,&rear,tempnode->right);
		tempnode=dequeue(queue,&front);
	}
}
int main()
{
  struct node *root = newnode(1);
  root->left        = newnode(2);
  root->right       = newnode(3);
  root->left->left  = newnode(4);
  root->left->right = newnode(5); 
 
  printf("Level Order traversal of binary tree is \n");
  printLevelOrder(root);
 // printlevelorder(root);
 
  getchar();
  return 0;
}


