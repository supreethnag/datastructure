#include<stdio.h>
#include<stdlib.h>
 
/* A binary tree tNode has data, pointer to left child
   and a pointer to right child */
struct tNode
{
   int data;
   struct tNode* left;
   struct tNode* right;
};
 
/* Function to traverse binary tree without recursion and 
   without stack */
void MorrisTraversal(struct tNode *root)
{
  struct tNode *current,*pre;
 
  if(root == NULL)
     return; 
 
  current = root;
  while(current != NULL)
  { 
	printf("current is %d\n",current->data);                
    if(current->left == NULL)
    {
      printf("traversed is %d\n ", current->data);
      current = current->right;      
    }    
    else
    {
      
	pre = current->left;
      
	while(pre->right != NULL && pre->right!=current)
        pre = pre->right;
	 
      printf(" pre is %d\n ", pre->data);
      if(pre->right == NULL)
      {
        pre->right = current;
        current = current->left;
      }
	
	else
	{
		pre->right=NULL;
		printf("traversed inside is %d\n ",current->data);
		current=current->right;
	}
             
    } /* End of if condition current->left == NULL*/
  } /* End of while */
}
 
/* UTILITY FUNCTIONS */
/* Helper function that allocates a new tNode with the
   given data and NULL left and right pointers. */
struct tNode* newtNode(int data)
{
  struct tNode* tNode = (struct tNode*)
                       malloc(sizeof(struct tNode));
  tNode->data = data;
  tNode->left = NULL;
  tNode->right = NULL;
 
  return(tNode);
}
 
/* Driver program to test above functions*/
int main()
{
 
  /* Constructed binary tree is
            1
          /   \
        2      3
      /  \
    4     5
  */
  struct tNode *root = newtNode(1);
  root->left        = newtNode(2);
  root->right       = newtNode(3);
  root->left->left  = newtNode(4);
  root->left->right = newtNode(5); 
 
  MorrisTraversal(root);
 
  getchar();
  return 0;
}
