#include<stdio.h>
#include<stdlib.h>
#include<iostream>
struct node{
int n;
struct node *next;
};
void printlist(struct node*);
int countNodes(struct node *s)
{
    int count = 0;
    while (s != NULL)
    {
        count++;
        s = s->next;
    }
    return count;
}
void swapnthnode(struct node **head,int n)
{
	int num=countNodes(*head);
	if(num<n)
		return;
	if(2*n-1==n)
		return;	

	int count=1;
	struct node *xprev,*yprev,*current,*fastptr,*slowptr;
	current=*head;
	xprev=yprev=NULL;
	while(count<n && current!=NULL)
	{
		xprev=current;
		current=current->next;
		count++;
	}	
	printf("x is %d",current->n);
	fastptr=current;
	slowptr=*head;
	
	while(fastptr->next!=NULL)
	{
		fastptr=fastptr->next;
		yprev=slowptr;
		slowptr=slowptr->next;
	}
	printf("y is %d",slowptr->n);
	
	if(xprev)
		xprev->next=slowptr;
	if(yprev)
		yprev->next=current;
	struct node *temp=current->next;
	current->next=slowptr->next;
	slowptr->next=temp;
	
	if(n==1)
	*head=slowptr;
	if(n==num)
	*head=current;
		
}
void push(int data,struct node **noderef)
{
	struct node* newnode=(struct node *)malloc(sizeof(struct node));
	newnode->n=data;
	newnode->next=(*noderef);
	(*noderef)=newnode;
}

struct node * sortedmerge(struct node *a,struct node *b)
{
	struct node *result=NULL;
	if(a==NULL)
		return(b);
	if(b==NULL)
		return(a);
	if(a->n<b->n)
	{
		result=a;
		result->next=sortedmerge(a->next,b);
	}
	else
	{	
		result=b;
                result->next=sortedmerge(a,b->next);	
	}
	return(result);
}

void frontbacksplit(struct node *source, struct node **frontref,struct node **backref)
{
	struct node *slow,*fast;
	if(source==NULL || source->next==NULL)
	{
		*frontref=source;
		*backref=NULL;
	}
	else
	{	
		slow=source;
		fast=source->next;
	}	
	while(fast!=NULL)
	{
		fast=fast->next;
		if(fast!=NULL)
		{
			slow=slow->next;
			fast=fast->next;
		}
	}		
	*frontref=source;
	*backref=slow->next;
	slow->next=NULL;
}

void mergesort(struct node **headref)
{
        struct node *head=*headref;
        struct node *a;
        struct node *b;

        if((head==NULL) || (head->next==NULL))
                return;
        frontbacksplit(head,&a,&b);
        mergesort(&a);
        mergesort(&b);
        *headref=sortedmerge(a,b);
}

int getNth(struct node *head,int index)
{
	int count=0;
	struct node *current=head;
	while(current!=NULL)
	{
		if(count==index)
		{
			return(current->n);
		}
		current=current->next;
		count++;
	}
	printf("node not found \n");
}

struct node * _knodealtreverse(struct node *head,int k,bool flag)
{
	struct node *cur=head;
        struct node *prev=NULL;
        struct node *next;
        int count=0;
        if(head==NULL)
		return NULL;
	
	while(cur!=NULL && count<k)
        {
                next=cur->next;
		if(flag==true)
			cur->next=prev;
                prev=cur;
                cur=next;
                count++;
        }
	
	if(flag==true)
	{
		head->next=_knodealtreverse(cur,k,!flag);
		return prev;	
	}
	else
	{
		prev->next=_knodealtreverse(cur,k,!flag);
		return head;
	}
	
}
struct node * reversealtknodes(struct node *head,int k)
{
	struct node *cur=head;
	struct node *prev=NULL;
	struct node *next;
	int count=0;
	while(cur!=NULL && count<k)
	{
		next=cur->next;
		cur->next=prev;
		prev=cur;
		cur=next;	
		count++;
	}
	if(head!=NULL)
		head->next=cur;
	int j=0;
	while(cur!=NULL && j<k-1)
	{
		cur=cur->next;
		j++;
	} 
	if(cur!=NULL)
		cur->next=reversealtknodes(cur->next,k);	
	return prev;		
}

void printlist(struct node *head)
{
        struct node *current=head;
        while(current!=NULL)
        {
                printf("%d \n",current->n);
		current=current->next;
        }
}


void deleteNode(struct node *nodeptr)
{
	struct node *temp=nodeptr->next;
	nodeptr->n=temp->n;
	nodeptr->next=temp->next;
	free(temp);
}

void reverse(struct node **head)
{
	struct node *current,*prev,*next;
	prev=NULL;
	current=*head;
	while(current!=NULL)
	{
		next=current->next;
		current->next=prev;
		prev=current;
		current=next;
	}
	*head=prev;
}
void midnode(struct node *head)
{
	struct node *slowptr,*fastptr;
	slowptr=head;
	fastptr=head;
	while(fastptr->next!=NULL)
	{
		fastptr=fastptr->next;
		if(fastptr->next!=NULL)		
		{
			slowptr=slowptr->next;
			fastptr=fastptr->next;
		}
	}
	printf("mid element is %d",slowptr->n);
}

void midnode1(struct node *head)
{
	int count=0;
	struct node *mid=head;
	while(head!=NULL)
	{
		if(count & 1)
			mid=mid->next;
		count++;
		head=head->next;
	}
	printf("middle element is %d",mid->n);
}

void nthfromlast(struct node *head,int nth)
{
	int count=0;
	struct node *lastref=head;
	struct node *ref=head;
	while(count<nth)
	{
		lastref=lastref->next;
		count++;
	}
	while(lastref!=NULL)
	{
		ref=ref->next;
		lastref=lastref->next;
	}
	printf("nth element from last is %d",ref->n);
}

bool isPalindromeUtil(struct node **left, struct node *right)
{
	if(right==NULL)
		return true;
	
	bool isp=isPalindromeUtil(left,right->next);

	if(isp==false)
		return false;
	bool isp1=((*left)->n == right->n);
	*left=(*left)->next;
		
	return isp1;		
}
bool isPalindrome(struct node *head)
{
	 isPalindromeUtil(&head,head)?printf("Is palindrome\n"):printf("Not palindrome\n");
}

int main()
{
	struct node *head=NULL;
	push(7,&head);
	push(2,&head);
	push(3,&head);
	push(4,&head);
	push(5,&head);
        push(8,&head);
	push(1,&head);
	printlist(head);
	mergesort(&head);
	printlist(head);
/*	swapnthnode(&head,3);
	printf("\n");
	printf("\n");
	printlist(head);
	isPalindrome(head);
	printlist(head);	
	reverse(&head);
	printlist(head);
	nthfromlast(head,3);
	midnode1(head);
	int x=getNth(head,2);
	printf("2nd element is %d\n",x);
	printlist(head);
	printf("deleting the first node\n");
	deleteNode(head);
	printlist(head);*/
	return(0);
}
