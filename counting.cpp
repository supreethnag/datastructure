#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int n;
    cin>>n;    
    int a[n],count[n];
    for(int i=0;i<n;i++) count[i]=0;    
     
    for(int i=0;i<n;i++)
    {    
        cin>>a[i];
        count[a[i]]++;
    }
    cout<<count[0];    
    for(int i=1;i<n;i++) cout<<" "<<count[i];
    
    return 0;
}

