#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
void partition(vector <int>  a) {

    int pivot=a[0];
    int i,j,n;
    n=a.size();
    i=1,j=n-1;
    while(i<j)
    {        
        while(a[i]<pivot)
            i++;
        while(a[j]>pivot)
            j--;
        if(i<j)
        {
            int tmp;
            tmp=a[i];
            a[i]=a[j];
            a[j]=tmp;
        }    
    }
    a[0]=a[j];
    a[j]=pivot;
    
    for(i=0;i<n;i++)
        cout<<a[i];

}
int main(void) {
   vector <int>  _ar;
   int _ar_size;
cin >> _ar_size;
for(int _ar_i=0; _ar_i<_ar_size; _ar_i++) {
   int _ar_tmp;
   cin >> _ar_tmp;
   _ar.push_back(_ar_tmp); 
}

partition(_ar);
   
   return 0;
}

