#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void swap(int* a, int* b)
{
    int t = *a;
    *a = *b;
    *b = t;
}
void quicksort(vector <int>  &a, int l,int r) {
    if(l>=r)
        return ;

    int p = a[r];
    int i=l-1;
    for(int j=l;j<r;j++)
    {
        if(a[j]<=p)    
        {
            i++;
            swap(&a[i], &a[j]);
        }
            
    }
     swap(&a[i+1], &a[r]);
    quicksort(a,l,i);
    quicksort(a,i+2,r);
}


int main() {
    	int n,m,i,j;
	cin>>n;
	vector<int> a;
	for(i=0;i<n;i++){
		int tmp;
		cin>>tmp;
		a.push_back(tmp);
	}
	cin>>m;
        vector<int> b;
        for(i=0;i<m;i++)
        {
                int tmp;
                cin>>tmp;
                b.push_back(tmp);
        }
	quicksort(a,0,a.size()-1);
	quicksort(b,0,b.size()-1);
	int ref=b[0];
	i=0;
	while(i<m)
	{
		int count1=0;
		int count2=0;
		for(j=i;j<m;j++)
		{
			if(b[j]==ref)
				count1++;
			else
				break;
		}
		for(j=0;j<n;j++)
		{
			if(a[j]==ref)	
				count2++;
			
		}
		if(count2<count1)
			cout<<ref<<" ";
		i=i+count1;	
	}	
	return 0;
}

