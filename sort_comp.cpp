#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int insertion_sort_shifts;
int quicksort_swaps;    

void insertionSort(int ar_size, vector <int>   ar) {
     int i,j,k,tmp;

     for(i=1;i<ar_size;i++)
     {
        tmp=ar[i];	
        for(j=i;j>=0;j--)
        {
            if(tmp<ar[j-1])
            {
                ar[j]=ar[j-1];
                insertion_sort_shifts++;
            }
            else
            {
		insertion_sort_shifts++;
                ar[j]=tmp;
                break;
            }
        }      
     }
}

void swap(int* a, int* b)
{
    quicksort_swaps++;
    int t = *a;
    *a = *b;
    *b = t;
}
void quicksort(vector <int>  &a, int l,int r) {
    if(l>=r)
        return ;

    int p = a[r];
    int i=l-1;
    for(int j=l;j<r;j++)
    {
        if(a[j]<=p)    
        {
            i++;
            swap(&a[i], &a[j]);
        }
            
    }
     swap(&a[i+1], &a[r]);    
    quicksort(a,l,i);
    quicksort(a,i+2,r);
}

int main() {
    vector <int>  _ar;
    int _ar_size;
    cin >> _ar_size;
    for(int _ar_i=0; _ar_i<_ar_size; _ar_i++)
    {
        int _ar_tmp;
        cin >> _ar_tmp;
        _ar.push_back(_ar_tmp);
    }
    insertion_sort_shifts=quicksort_swaps=0;
    insertionSort(_ar_size,_ar);
    quicksort(_ar,0,_ar_size-1);
    cout<<insertion_sort_shifts-quicksort_swaps;
   return 0;
}

