#include<stdio.h>
#include<stdlib.h>
struct node
{
	int data;
	struct node *next;
	struct node *child;
};

void printlist(struct node *root)
{
	while(root!=NULL)
	{
		printf("%d ",root->data);
		root=root->next;
	}
	printf("\n");
}

struct node *createList(int *arr, int n)
{
    struct node *head = NULL;
    struct node *p;
 
    int i;
    for (i = 0; i < n; ++i) {
        if (head == NULL)
            head = p = (struct node *)malloc(sizeof(*p));
        else {
            p->next = (struct node *)malloc(sizeof(*p));
            p = p->next;
        }
        p->data = arr[i];
        p->next = NULL;
	p->child = NULL;
    }
    return head;
}

struct node *createList(void)
{
    int arr1[] = {10, 5, 12, 7, 11};
    int arr2[] = {4, 20, 13};
    int arr3[] = {17, 6};
    int arr4[] = {9, 8};
    int arr5[] = {19, 15};
    int arr6[] = {2};
    int arr7[] = {16};
    int arr8[] = {3};
 
    /* create 8 linked lists */
    struct node *head1 = createList(arr1, sizeof(arr1)/4);
    struct node *head2 = createList(arr2, sizeof(arr2)/4);
    struct node *head3 = createList(arr3, sizeof(arr3)/4);
    struct node *head4 = createList(arr4, sizeof(arr4)/4);
    struct node *head5 = createList(arr5, sizeof(arr5)/4);
    struct node *head6 = createList(arr6, sizeof(arr6)/4);
    struct node *head7 = createList(arr7, sizeof(arr7)/4);
    struct node *head8 = createList(arr8, sizeof(arr8)/4);
 
 
    /* modify child pointers to create the list shown above */
    head1->child = head2;
    head1->next->next->next->child = head3;
    head3->child = head4;
    head4->child = head5;
    head2->next->child = head6;
    head2->next->next->child = head7;
    head7->child = head8;
 
 
    /* Return head pointer of first linked list.  Note that all nodes are
       reachable from head1 */
    return head1;
}

void flatten(struct node *root)
{
	struct node *cur=root;
	struct node *tail=NULL;
	if(root==NULL)
		return;
	struct node *tmp=root;
	while(tmp->next!=NULL)
		tmp=tmp->next;
	tail=tmp;
	while(cur!=NULL)
	{
		if(cur->child)
		{
			tail->next=cur->child;
			while(tail->next!=NULL)
			{
				tail=tail->next;
			}
		}
		cur->child=NULL;
		cur=cur->next;
	}
}

int main(void)
{
    struct node *head = NULL;
    head = createList();
    flatten(head);
    printlist(head);
    return 0;
}


