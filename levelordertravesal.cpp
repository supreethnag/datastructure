#include<iostream>
#include<queue>

#include<stdlib.h>

using namespace std;
struct node 
{
	int data;
	struct node *left,*right;
};

struct node* newnode(int data)
{
  struct node* node = (struct node*)
                       malloc(sizeof(struct node));
  node->data = data;
  node->left = NULL;
  node->right = NULL;
  return(node);
}


void printlevelorder1(struct node *root)
{
	queue<struct node *> myqueue;
	struct node *tempnode=root;
	while(tempnode)
	{
		cout<<tempnode->data<<" is the traversed node"<<endl;
		if(tempnode->left)
			myqueue.push(tempnode->left);
		if(tempnode->right)
			myqueue.push(tempnode->right);
		if(myqueue.size()==0)
			return;
		tempnode=myqueue.front();
		myqueue.pop();
		cout<<"tempnode data is "<<tempnode->data<<endl;
	}
}


int main()
{
  struct node *root = newnode(1);
  root->left        = newnode(2);
  root->right       = newnode(3);
  root->left->left  = newnode(4);
  root->left->right = newnode(5); 
 
  printf("Level Order traversal of binary tree is \n");
 //printLevelOrder(root);
 // printlevelorder(root);
 	printlevelorder1(root);
  return 0;
}


