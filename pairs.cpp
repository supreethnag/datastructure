#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int t,n;
    cin>>t;
    while(t--)
    {
        cin>>n;
        int i,j,a[n];
        for(i=0;i<n;i++)
            cin>>a[i];
	int count=0;
        for(i=0;i<n;i++)
	{
            	for(j=i+1;j<n;j++)
		{
               		if(a[i]==a[j])
				count+=2;
		}
	}
	cout<<count<<endl;
    }       
    return 0;
}

