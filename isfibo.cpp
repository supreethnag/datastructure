#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int *a,n,i,fib1=0,fib2=1,x;
    cin>>n;
    a= new int[n];
    for(i=0;i<n;i++)
        cin>>a[i];  
    
    for(i=0;i<n;i++){
        if(a[i]==0 || a[i]==1)
        {
            cout<<"IsFibo"<<endl;
            continue;
        }    
        while(1){
            x=fib1+fib2;
            if(x>a[i]){
                cout<<"IsNotFibo"<<endl;
                break;
            }                    
            else if(a[i]==x){
                cout<<"IsFibo"<<endl;
                break;
            }
            fib1=fib2;
            fib2=x;
        }
    }
    
    return 0;
}

