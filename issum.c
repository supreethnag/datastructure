#include <stdio.h>
#include <stdlib.h>
 
struct node
{
    int data;
    struct node* left;
    struct node* right;
};
 
struct node* newNode(int data)
{
  struct node* node =
      (struct node*)malloc(sizeof(struct node));
  node->data = data;
  node->left = NULL;
  node->right = NULL;
  return(node);
}
int isSumProperty(struct node *node)
{
	int lchild=0,rchild=0;
	if(!node || (!node->left && !node->right))
		return 1;
	else
	{
		if(node->left)
			lchild=node->left->data;
			
		if(node->right)
			rchild=node->right->data;
		if(node->data == (lchild+rchild) && isSumProperty(node->left) && isSumProperty(node->right))
			return 1;
		else
			return 0;
	}
}

 
int main()
{
  struct node *root  = newNode(10);
  root->left         = newNode(8);
  root->right        = newNode(2);
  root->left->left   = newNode(3);
  root->left->right  = newNode(5);
  root->right->right = newNode(2);
  if(isSumProperty(root))
    printf("The given tree satisfies the children sum property ");
  else
    printf("The given tree does not satisfy the children sum property ");
 
  getchar();
  return 0;
}
