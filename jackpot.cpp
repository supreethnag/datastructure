#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int t,i;
    cin>>t;
    while(t--)
    {
        int n,a,b,div,i,j,num;
        cin>>n>>a>>b;
	num=int(pow(2.0,double(n-1)));
        long long x[num];
        div=num;
        bool flag=1;
	for(i=0;i<num;i++)
		x[i]=0;

        for(i=0;i<n-1;i++)
        {   
            div=div/2;
            int count=0;
            for(j=0;j<num;j++)
            {   
                count=count+1;
                if(count==div)
                { 
                    flag=!flag;
                    count=0; 
                }       
                if(flag==0)                
                    x[j]+=a;
                else
                    x[j]+=b;
            }
        }    
	
	long temp;
	for (int i=0; i < num; i++){
		j = i;
		
		while (j > 0 && x[j] < x[j-1]){
			  temp = x[j];
			  x[j] = x[j-1];
			  x[j-1] = temp;
			  j--;
			  }
		}
	div=-1;
	for(i=0;i<num;i++)
	{
		if(div==x[i])
		{	
			continue;
		}
		else
		{
	     		cout<<x[i]<<" ";
			div=x[i];			
		}
	}
	cout<<endl;
    }
    return 0;
}

