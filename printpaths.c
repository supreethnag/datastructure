#include<stdio.h>
#include<stdlib.h>
struct node
{
   int data;
   struct node* left;
   struct node* right;
};
	
void printArray(int ints[], int len) 
{
  int i;
  for (i=0; i<len; i++) 
  {
    printf("%d ", ints[i]);
  }
  printf("\n");
}    

void printPathsRecur(struct node *node,int path[],int len)
{
	if(!node)
		return;
	path[len]=node->data;
	len++;
	
	if(!node->left && !node->right)
	{
		printArray(path,len);
	}
	else
	{
		printPathsRecur(node->left,path,len);	
		printPathsRecur(node->right,path,len);
	}
}
void printPaths(struct node* node) 
{
  int path[1000];
  printPathsRecur(node, path, 0);
}
 
/* utility that allocates a new node with the
   given data and NULL left and right pointers. */  
struct node* newnode(int data)
{
  struct node* node = (struct node*)
                       malloc(sizeof(struct node));
  node->data = data;
  node->left = NULL;
  node->right = NULL;
  
  return(node);
}
int main()
{ 
  
  struct node *root = newnode(10);
  root->left        = newnode(8);
  root->right       = newnode(2);
  root->left->left  = newnode(3);
  root->left->right = newnode(5);
  root->right->left = newnode(2);
  
  printPaths(root);
  
  getchar();
  return 0;
}
